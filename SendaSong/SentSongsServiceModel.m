//
//  SentSongsServiceModel.m
//  SendaSong
//
//  Created by isaac ojeda on 29/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SentSongsServiceModel.h"


@implementation SentSongsServiceModel

#pragma mark - Public methods
-(void) GetFirstSongsByUserId:(NSString *)userId
              completionBlock:(CompletionBlock)completionBlock
{
    NSString * queryString = [NSString stringWithFormat:@"songs?userId=%@", userId];
    
    WebRequester *requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                          method:queryString
                                                        delegate:nil
                                                          params:nil];

    [requester setWebserviceCallCompleted:completionBlock];
    [requester makeGenericCall:nil httpMethod:@"GET"];
}

-(void) GetSongById:(NSInteger)songId
    completionBlock:(CompletionBlock)completionBlock
{
    NSString * queryString = [NSString stringWithFormat:@"songs/%ld", (long)songId];
    WebRequester * requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                           method:queryString
                                                         delegate:nil
                                                           params: nil];
    
    [requester setWebserviceCallCompleted:completionBlock];
    [requester makeGenericCall:nil httpMethod:@"GET"];
}

-(void) GetSongsAfterSongId:(NSInteger)songId
                     userId:(NSString *)userId
            completionBlock:(CompletionBlock)completionBlock
{
    NSString *queryString = [NSString stringWithFormat:@"songs?SongIdStart=%ld&uid=%@",(long)songId, userId];
    WebRequester *requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                          method:queryString
                                                        delegate:nil
                                                          params: nil];
    
    [requester setWebserviceCallCompleted:completionBlock];
    [requester makeGenericCall:nil httpMethod:@"GET"];
}

-(void) SetSongAsReaded:(NSInteger)songId completionBlock:(CompletionBlock)completionBlock
{
    NSString *uriRequest = [NSString stringWithFormat:@"songs/%ld/Readed", (long)songId];
    WebRequester *requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                          method:uriRequest
                                                        delegate:nil
                                                          params:nil];
    [requester setWebserviceCallCompleted:completionBlock];
    [requester makeGenericCall:nil httpMethod:@"PUT"];
}

+(NSURL*) CreateSongURI:(NSInteger)songId
{
    NSString* url = [NSString stringWithFormat:@"%@songs?songId=%ld", [WebRequester webServiceUrl], (long)songId];
    
    return [[NSURL alloc] initWithString:url];
}
-(void)PostSong:(RdioTrack *)trackModel
   withSenderId:(NSString *)senderId
  andReceiverId:(NSString *)receiverId
    withMessage:(NSString *)message
completionBlock:(CompletionBlock)completionBlock
{
    
        NSString *uriRequest = [NSString stringWithFormat:@"songs"];
        WebRequester *requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                              method:uriRequest
                                                            delegate:nil
        
                                                              params:nil];
        NSString *albumUrl = [[trackModel AlbumImageURL400] absoluteString];
        //Rdio data
        NSArray *rdioKeys = [[NSArray alloc] initWithObjects:@"Artist",@"Name",@"RdioKey",@"AlbumUrl", nil];
        NSArray *rdioValues = [[NSArray alloc] initWithObjects:[trackModel ArtistName],[trackModel TrackName], [trackModel TrackId], albumUrl,nil];
        
        NSDictionary *rdioData = [[NSDictionary alloc] initWithObjects:rdioValues forKeys:rdioKeys];
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"SenderUserId",@"ReceiverUserId",@"Message",@"SongMetaData", nil];
        NSArray *values = [[NSArray alloc] initWithObjects:senderId,receiverId,message,rdioData, nil];
        
        NSDictionary *dataToSend = [[NSDictionary alloc]initWithObjects:values forKeys:keys];
        
        [requester setWebserviceCallCompleted:completionBlock];
        [requester makeGenericCall:dataToSend httpMethod:@"POST"];
}
@end
