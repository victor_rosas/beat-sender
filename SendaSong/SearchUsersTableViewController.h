//
//  SearchUsersTableViewController.h
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchUsersTableViewController : UITableViewController<UISearchBarDelegate>

@end
