//
//  InboxTableViewController.m
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "InboxTableViewController.h"
#import "SentSongsServiceModel.h"
#import "AppDelegate.h"
#import "SongDetailViewController.h"
#import "MBProgressHUD.h"

@interface InboxTableViewController ()
{
    BOOL firstTime;
}

@end

@implementation InboxTableViewController

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Custom init
        
        self->firstTime = YES;
    }
    
    return self;
}


#pragma mark - View Events
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self->songs = [[NSMutableArray alloc] init];
    self->currentUser = [((AppDelegate*)[[UIApplication sharedApplication] delegate]) CurrentUser];
    
    // Pull to refresh logic
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self
                       action:@selector(pullToRefreshAction)
             forControlEvents:UIControlEventValueChanged];
    
    [self setRefreshControl:refreshControl];
    
    // Register for push notifications
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if (self->firstTime) {
        // Primera vez que se muestra la vista, mostramos los ùltimos beats del usuario
        [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
        
        SentSongsServiceModel *serviceModel = [[SentSongsServiceModel alloc] init];
        
        [serviceModel GetFirstSongsByUserId:[currentUser UserId]
                            completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode)
         {
             NSLog(@"Recibido con :%ld", (long)statusCode);
             
             [MBProgressHUD hideHUDForView:[self view] animated:YES];
             
             if (statusCode == 200) {
                 // TODO: Load data in Table View
                 NSArray * tempSongs = (NSArray*) myJson;
                 
                 for (int i = 0; i < [tempSongs count]; i++) {
                     NSDictionary *songData = [tempSongs objectAtIndex:i];
                     
                     SongModel *songModel = [[SongModel alloc] initWithData:songData];
                     
                     [songs addObject:songModel];
                 }
                 
                 // Reload Table View
                 [[self songsTableView] reloadData];
             }
         }];
        
        self->firstTime = NO;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - TableView DataSource
/**
 * Cuantas secciones tiene el tableView
 */
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/**
 * Cuantos rows tiene cada section
 */
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [songs count];
}

/**
 * Contenido de cada celda
 */
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * MyIdentifier = @"MyIdentifierYeah";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    SongModel *currentSong = [songs objectAtIndex:[indexPath row]];
    
    [[cell textLabel ]setText:[currentSong UserName]];
    [[cell detailTextLabel] setText:[currentSong SentDate]];
    [cell setAccessoryType: UITableViewCellAccessoryDisclosureIndicator];
    
    // Syle
    if ([currentSong IsReaded]) {
        [self setCellAsReaded:cell];
    }else{
        [self setCellAsDefault:cell];
    }

    return cell;
}
/**
 * Al seleccionar una celda...
 */
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Se marca como celda leida
    [self setCellAsReaded:[tableView cellForRowAtIndexPath:indexPath]];
    // Se obtiene cual canción se seleccionó
    NSInteger row = [indexPath row];
    self->selectedSong = [songs objectAtIndex:row];
    [self->selectedSong setIsReaded:YES];
    
    // Segue...
    [self performSegueWithIdentifier:@"inboxToDetail" sender:self];
}

 #pragma mark - Navigation
/**
 * Al pasar de una vista a otra...
 */
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {

     if (self->selectedSong) {
         SongDetailViewController *nextController = [segue destinationViewController];
         nextController->Song = self->selectedSong;
         
         self->selectedSong = nil;
     }
 }
#pragma mark - Private methods
/**
 * Que hacer al momento que se quiere 
 * actualizar el table view
 */
-(void) pullToRefreshAction
{
    // Refresh data
    NSInteger lastId = 1;
    
    if ([self->songs count] > 0) {
        lastId = [((SongModel*)[self->songs objectAtIndex:0]) SongId];
    }else{
        // Si no hay canciones cargadas es porque el usuario aun no ha recibido una, si hace pull to refresh
        // se buscará desde la primer canción de la historia, pero ser filtrará por el usuario logueado
        lastId = 1;
    }

    
    NSLog(@"Buscando canciones despues de %ld", (long)lastId);
    
    SentSongsServiceModel *serviceModel = [[SentSongsServiceModel alloc] init];
    [serviceModel GetSongsAfterSongId:lastId userId:[self->currentUser UserId] completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
        // TODO: Handle response
        
        NSLog(@"Recibido con: %ld", (long)statusCode);
        if (statusCode == 200) {
            // Se obtienen las nuevas canciones y se agregan a songs (utilizado por la tabla
            // para llenar con datos)
            
            NSArray *newSongs = (NSArray*) myJson;
            
            for (int i = 0; i < [newSongs count]; i++) {
                NSDictionary *songData = [newSongs objectAtIndex:i];
                SongModel *songModel = [[SongModel alloc] initWithData:songData];
                [songs insertObject:songModel atIndex: 0];
                
                [[self songsTableView] reloadData];
            }
        }else{
            // TODO: Handle error here...
            NSLog(@"ERROR :(");
        }
        
        
        [[self refreshControl] endRefreshing];
    }];
}
/**
 * Marca una celda como leida (la pone gris)
 */
-(void) setCellAsReaded:(UITableViewCell*) cell
{
    UIColor *gray = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    
    [[cell textLabel] setFont:[UIFont fontWithName:@"Arial" size:15]];
    [[cell textLabel]setTextColor:gray];
    [[cell detailTextLabel] setTextColor:gray];
}

-(void) setCellAsDefault:(UITableViewCell*) cell
{
    UIColor *gray = [UIColor blackColor];
    
    [[cell textLabel] setFont:[UIFont fontWithName:@"Arial" size:16]];
    [[cell textLabel]setTextColor:gray];
    [[cell detailTextLabel] setTextColor:gray];
}

@end
