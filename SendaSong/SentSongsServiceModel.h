//
//  SentSongsServiceModel.h
//  SendaSong
//
//  Created by isaac ojeda on 29/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRequester.h"
#import "RdioTrack.h"

/**
 * Llamadas CRUD de cancioness
 */
@interface SentSongsServiceModel : NSObject


/**
 * Obtiene las canciones más recientes de un usuario
 * @params userId Id del usuario
 */
-(void) GetFirstSongsByUserId: (NSString*) userId
              completionBlock: (CompletionBlock) completionBlock;
/**
 * Obtiene los datos completos de una canción
 * @params songId Id de la canción
 */
-(void) GetSongById:(NSInteger) songId
    completionBlock: (CompletionBlock) completionBlock;

/**
 * Obtiene las canciones despues del songId dado
 * @params songId id de la canción
 */
-(void) GetSongsAfterSongId: (NSInteger) songId
                     userId: (NSString*) userId
            completionBlock: (CompletionBlock) completionBlock;

/**
 * Marca como leido una canción
 * @params songId Id de la canción
 */
-(void) SetSongAsReaded: (NSInteger) songId
        completionBlock: (CompletionBlock) completionBlock;

/**
 * Construye un URL para descargar los bytes de una canción
 * @param songId Id de la canción
 */
+(NSURL*) CreateSongURI: (NSInteger) songId;
/**
 * Da de alta una nueva canción
 */
-(void) PostSong:(RdioTrack*) trackModel
    withSenderId:(NSString*) senderId
   andReceiverId:(NSString*) receiverId
     withMessage:(NSString*) message
 completionBlock:(CompletionBlock) completionBlock;

@end
