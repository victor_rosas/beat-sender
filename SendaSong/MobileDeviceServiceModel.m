//
//  MobileDeviceServiceModel.m
//  BeatSender
//
//  Created by isaac ojeda on 1/17/15.
//  Copyright (c) 2015 Isaac Ojeda. All rights reserved.
//

#import "MobileDeviceServiceModel.h"


@implementation MobileDeviceServiceModel


-(void) registerDeviceToken:(NSString *)token
                 withUserId:(NSString *)userId
            completionBlock:(CompletionBlock)block
{
    WebRequester *request = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                        method:@"mobiledevices"
                                                      delegate:nil
                                                        params: nil];
    [request setWebserviceCallCompleted:block];
    
    NSDictionary* dataToSend = @{ @"UserId":userId, @"DeviceType":@0, @"PushToken":token };
    
    [request makeGenericCall:dataToSend httpMethod:@"POST"];
    
}

-(void) deleteDeviceToken:(NSString *)token
          completionBlock:(CompletionBlock)block
{
    
    NSString *url = [[NSString alloc] initWithFormat:@"mobiledevices?token=%@",token];
    WebRequester *request = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                        method:url
                                                      delegate:nil
                                                        params:nil];
    
    [request setWebserviceCallCompleted:block];
    
    [request makeGenericCall:nil httpMethod:@"DELETE"];
}

@end
