//
//  BeatColorsHelper.h
//  BeatSender
//
//  Created by isaac ojeda on 19/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BeatColorsHelper : NSObject

+(UIColor*) mainColor;
+(UIColor*) secondaryColor;
+(UIColor*) beatBlueColor;
@end
