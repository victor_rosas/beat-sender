//
//  UICustomFriendTableViewCell.m
//  BeatSender
//
//  Created by Isaac Ojeda on 4/20/15.
//  Copyright (c) 2015 Isaac Ojeda. All rights reserved.
//

#import "UICustomFriendTableViewCell.h"

@implementation UICustomFriendTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
