//
//  SongDetailViewController.h
//  SendaSong
//
//  Created by isaac ojeda on 29/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SongModel.h"
#import <AVFoundation/AVFoundation.h>
#import <Rdio/Rdio.h>
/**
 * Muestra el detalle de una canción
 */
@interface SongDetailViewController : UIViewController/*<RdioDelegate,RDPlayerDelegate>*/
{
    @public
        /** Canción seleccionada en el table view anterior */
        SongModel *Song;
        
}
/** AudioPlayer que reproduce la canción */
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
/** Label que muestra un mensaje */
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
/** */
@property (weak, nonatomic) IBOutlet UILabel *lblTrackName;
/** */
@property (weak, nonatomic) IBOutlet UILabel *lblAlbumName;
/**
 *
 */
@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;

@end
