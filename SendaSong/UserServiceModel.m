//
//  UserServiceModel.m
//  BeatSender
//
//  Created by isaac ojeda on 03/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "UserServiceModel.h"

@implementation UserServiceModel


-(void) followUser:(NSString *)followingId
            withId:(NSString *)followerId
   completionBlock:(CompletionBlock)completionBlock
{
    WebRequester* requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                          method:@"users/follow"
                                                        delegate:nil
                                                          params:nil];
    
    [requester setWebserviceCallCompleted:completionBlock];
    
    NSDictionary *dataToSend = @{ @"FollowerId" : followerId, @"FollowingId" : followingId};
    
    [requester makeGenericCall:dataToSend httpMethod:@"POST"];
}


-(void) getFriendsWithGuid:(NSString *)userId completionBlock:(CompletionBlock)completionBlock
{
    NSString* url = [NSString stringWithFormat:@"users/friends/%@",userId];
    
    WebRequester *requester = [[WebRequester alloc]initWithHost:[WebRequester webServiceUrl]
                                                         method:url
                                                       delegate:nil
                                                         params:nil];
    
    [requester setWebserviceCallCompleted:completionBlock];
    
    [requester makeGenericCall:nil httpMethod:@"GET"];
}

-(void) unFollowUser:(NSString *)followingId withId:(NSString *)followerId completionBlock:(CompletionBlock)completionBlock
{
    NSString *url = [NSString stringWithFormat:@"users/unfollow/%@/%@",followingId, followerId];
    
    WebRequester* requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl] method:url delegate:nil params:nil];
    [requester setWebserviceCallCompleted:completionBlock];
    [requester makeGenericCall:nil httpMethod:@"PUT"];
}
@end
