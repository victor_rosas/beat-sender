//
//  SongDetailViewController.m
//  SendaSong
//
//  Created by isaac ojeda on 29/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SongDetailViewController.h"
#import "MBProgressHUD.h"
#import "SentSongsServiceModel.h"
#import <Rdio/Rdio.h>
#import "ClientCredentials.h"

@interface SongDetailViewController ()
{
    Rdio * rdio; /** Wrapper de la API de Rdio */
    RDPlayer * player; /*Reproductor de audio*/
}

@end

@implementation SongDetailViewController

#pragma mark - View Events
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Mensaje
    [[self lblMessage] setText:[self->Song Message]];
    // rdio wrapper init ...
    self->rdio = [[Rdio alloc] initWithClientId:@"d5t3ybdaq5ejxfprkgfqazxsla"//CLIENT_ID
                                      andSecret:@"aEg6YVZRF6B3Rlxg4IdUPw"//CLIENT_SECRET
                                       delegate:nil];
    // Marcamos como leida la cancnión
    SentSongsServiceModel *serviceModel = [[SentSongsServiceModel alloc]init];
    [serviceModel SetSongAsReaded:[self->Song SongId] completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
        // TODO: Nothig.
    }];
    
    [[self navigationItem] setTitle:[self->Song ArtistName]];
    [[self lblTrackName] setText:[self->Song SongName]];

}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // reproducimos
    [rdio preparePlayerWithDelegate:nil];
    [rdio.player performSelector:@selector(play:) withObject:[self->Song RdioId] afterDelay:2.0];


   
    //[rdio.player performSelector:@selector(play:) withObject:[self->Song RdioId] afterDelay:2.0];
    
    //[[self->rdio player] play/*playSource*/: [self->Song RdioId]];
    
    //TODO Cargar esta imagen ASYNC
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //TODO: Load image async
        NSURL *albumUrl = [NSURL URLWithString:[self->Song AlbumUrl]];
        NSData *imageData = [NSData dataWithContentsOfURL: albumUrl];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [[self albumImageView]setImage: [UIImage imageWithData:imageData]];
        });
    });
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        
        if(rdio.player.state == RDPlayerStatePlaying){
            rdio.player.stop;
        }
        if (rdio.player.state == RDPlayerStateInitializing) {
            rdio.player.stop;
        }
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
