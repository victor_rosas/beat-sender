//
//  UICustomUserTableViewCell.m
//  BeatSender
//
//  Created by isaac ojeda on 03/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "UICustomUserTableViewCell.h"
#import "UserServiceModel.h"

@implementation UICustomUserTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) prepareCellForUserAdd
{
    if ([[self UserToFollow] Following]) {
        [[self lblFollowing] setHidden:NO];
        [[self btnFollow] setHidden:YES];
    }else{
        [[self btnFollow] setHidden:NO];
        [[self lblFollowing] setHidden:YES];
    }
}

- (IBAction)btnFollowClick:(id)sender {
    UserServiceModel *serviceModel = [[UserServiceModel alloc]init];
    
    [serviceModel followUser:[[self UserToFollow] UserId] withId:[[self UserFollowing]UserId] completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
        // TODO: Handle Response here
        
        if (statusCode == 200) {
            NSLog(@"Todo bien");
            [[self btnFollow] setHidden:YES];
            [[self lblFollowing] setHidden:NO];
        }
    }];
}
@end
