//
//  SearchTrackTableViewController.m
//  BeatSender
//
//  Created by isaac ojeda on 10/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SearchTrackViewController.h"
#import "UICustomTrackTableViewCell.h"
#import "MBProgressHUD.h"
#import "BeatColorsHelper.h"
#import "RdioTrack.h"
#import "ClientCredentials.h"
#import "AppDelegate.h"

@interface SearchTrackViewController ()
{
    NSMutableArray * searchResults;     /** Resultados de la búsqueda */
    NSString       * textToSearch;      /** Texto escrito en el search bar */
    Rdio             *rdio;              /** Wrapper de la API de Rdio */
    Rdio             *_rdio;              /** Wrapper de la API de Rdio */
    NSInteger        startCounter;      /** Guarda el offset de buscar en Rdio (como paginación)*/
    NSInteger        totalResults;      /** Número total de resultados de la búsqueda */
    BOOL             isLoadingMoreData; /** Determina si está cargando las canciones */
}

@end

@implementation SearchTrackViewController

/**
 * Basic init
 */
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // rdio wrapper init ...
        self->rdio = [[Rdio alloc] initWithClientId:@"d5t3ybdaq5ejxfprkgfqazxsla"//CLIENT_ID
                                          andSecret:@"aEg6YVZRF6B3Rlxg4IdUPw"//CLIENT_SECRET
                                           delegate:nil];
    }
    _rdio = [AppDelegate sharedRdio];
    [_rdio setDelegate:self];
    
    return self;
}
#pragma mark - UI Events
/**
 * Basic init
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _rdio = [AppDelegate sharedRdio];
    [_rdio setDelegate:self];
    
    self->searchResults = [[NSMutableArray alloc] init];
    self->startCounter = 0;
    self->totalResults = 0;
    self->isLoadingMoreData = NO;
}
/**
 * Al cargar la vista, hacemos focus en el textbox del searchbar
 */
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _rdio = [AppDelegate sharedRdio];
    [_rdio setDelegate:self];
    [[self searchBar] becomeFirstResponder];
}
/**
 *
 */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
/**
 * Ya no recuerdo que hace esto
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
/**
 * Cancela la búsqueda y se va al Inbox
 */
- (IBAction)cancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        // TODO: Cancelamos
        [[self ParentController] startOver];
    }];
}

#pragma mark - UISearchBarDelegate
/**
 * Al dar click en 'Search' busca canciones en Rdio
 */
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{

    [MBProgressHUD  showHUDAddedTo:[self view] animated:YES];

    self->textToSearch = [searchBar text];
    self->startCounter = 0;
    self->totalResults = 0;
    
    [self->searchResults removeAllObjects];
    
    // new implementation with Rdio API
    [self searchInRdio:self->textToSearch withStart:self->startCounter];
    
    [searchBar resignFirstResponder];
}

#pragma mark - Table view data source
/**
 * Cuantos rows tendrá la única sección que tendremos
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self->searchResults count];
}
/**
 * Determina la altura de las celdas
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
/**
 * Según los resultados de la API de Rdio los mostramos en el grid con 
 * celdas custom
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UICustomTrackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellTrackIdentifier" forIndexPath:indexPath];
    
    
    // Configure the cell...
    RdioTrack *currentTrack = [self->searchResults objectAtIndex:[indexPath row]];
    
    [[cell trackLabel] setText:[currentTrack TrackName]];
    [[cell artistLabel] setText:[currentTrack ArtistName]];
    
    if (![currentTrack IsLoaded]) {
        // Imagen default
        [[cell albumImageView] setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"default_cover" ofType:@"png"]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            //TODO: Load image async
            NSData *imageData = [NSData dataWithContentsOfURL:[currentTrack AlbumImageURL]];
            [currentTrack setAlbumImage:[UIImage imageWithData:imageData]];
            [currentTrack setIsLoaded:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [[cell albumImageView] setImage: [currentTrack AlbumImage]];
            });
        });
    }else{
        [[cell albumImageView] setImage:[currentTrack AlbumImage]];
    }

    return cell;
}
/**
 * Al seleccionar un Row se mandará a la vista padre para escucharla
 */
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UICustomTrackTableViewCell *selectedCell = (UICustomTrackTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    [selectedCell setSelected:NO];
    
    RdioTrack *selectedSong = [self->searchResults objectAtIndex:[indexPath row]];

    [self dismissViewControllerAnimated:YES completion:^{
        // TODO: Guardamos canción seleccionada
        
        [[self ParentController] setSelectedTrack:selectedSong];
        [[self ParentController] loadSongDetail];
    }];
}
/**
 * Sirve para hacer un pull down to refresh.
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
    {
        if (!self->isLoadingMoreData)
        {
            self->isLoadingMoreData = YES;
            
            [self pullUpToRefresh];
        }
    }
}

#pragma mark - RD API Request Delegate
/**
 * Callback que maneja la respuesta de la API de Rdio
 */

-(void)rdioRequest:(RDAPIRequest *)request didLoadData:(id)data
{
    //TODO Show results on grid
    
    NSDictionary *response = (NSDictionary*)data;
    NSArray * responseArray = [data objectForKey:@"results"];
    
    if (self->totalResults == 0) {
        // first time
        self->totalResults = [[response objectForKey:@"number_results"] integerValue];
    }
    
    for (int i=0; i<[responseArray count]; i++) {
        NSDictionary *jsonData = [responseArray objectAtIndex:i];
        
        RdioTrack *newTrack = [[RdioTrack alloc]initWithJson:jsonData];
        
        [self->searchResults addObject:newTrack];
    }
    
    // hide loading message
    [MBProgressHUD hideHUDForView:[self view] animated:YES];
    [[self tableView] reloadData];
    
    // Si no hubo resultados mostramos mensaje
    if ([self->searchResults count] == 0) {
        [[self noResultsLabel] setHidden: NO];
        [[self tableView] setHidden:YES];
    }else{
        [[self noResultsLabel] setHidden:YES];
        [[self tableView] setHidden:NO];
    }
    self->isLoadingMoreData = NO;
}

/**
 * Que hacer en caso de que la API de RDIO Falló
 */
-(void)rdioRequest:(RDAPIRequest *)request didFailWithError:(NSError *)error
{
    //TODO: Handle error here...
    NSLog(@"Error");
    NSLog(@"%@",error);
    
    [MBProgressHUD hideHUDForView:[self view] animated:YES];

    
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Beat Sender"
                                                       message:@"We lost the connection, try again"
                                                      delegate:nil
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles: nil];
        [alert show];
    }];
}

#pragma mark - Private methods
/**
 * Realiza una nueva búsqueda en la API
 */
-(void) pullUpToRefresh{
    
    if (self->totalResults <= self->startCounter) {
        // si startCounter es mayor al total de resultados ya no hay que seguir buscando
        self->isLoadingMoreData = NO;
        
        return;
    }
    
    // buscaremos de 10 en 10
    [MBProgressHUD showHUDAddedTo:[self view] animated:YES];

    self->startCounter = self->startCounter + 10;
    [self searchInRdio:self->textToSearch withStart:self->startCounter];
}
/**
 * Busca en el servicio de Rdio canciones con el término ingresado
 * @param searchTerm Término a buscar en la API
 * @param start página a obtener
 */
-(void) searchInRdio:(NSString*) searchTerm withStart:(NSInteger) start
{
    //rdio =[AppDelegate rdioInstance];
    //NSArray * objects = [[NSArray alloc]initWithObjects:searchTerm,@"Track",[NSString stringWithFormat:@"%d",start], nil];
    NSArray * objects = [[NSArray alloc]initWithObjects:@"Carlo gless",@"Track",@"0", nil];
    NSArray * keys = [[NSArray alloc]initWithObjects:@"query",@"types",@"start", nil];
    
    NSDictionary* params = [[NSDictionary alloc]initWithObjects: objects
                                                        forKeys: keys];

    //Declaracion de bloque para imprimir el resultado
    //
    //
    void (^searchresult)(NSDictionary* result);
    searchresult = ^void(NSDictionary *result){
        NSArray * responseArray = [result objectForKey:@"results"];
        
        if (self->totalResults == 0) {
            // first time
            self->totalResults = [[result objectForKey:@"number_results"] integerValue];
        }
        
        for (int i=0; i<[responseArray count]; i++) {
            NSDictionary *jsonData = [responseArray objectAtIndex:i];
            
            RdioTrack *newTrack = [[RdioTrack alloc]initWithJson:jsonData];
            
            [self->searchResults addObject:newTrack];
        }
        
        // hide loading message
        [MBProgressHUD hideHUDForView:[self view] animated:YES];
        [[self tableView] reloadData];
        
        // Si no hubo resultados mostramos mensaje
        if ([self->searchResults count] == 0) {
            [[self noResultsLabel] setHidden: NO];
            [[self tableView] setHidden:YES];
        }else{
            [[self noResultsLabel] setHidden:YES];
            [[self tableView] setHidden:NO];
        }
        self->isLoadingMoreData = NO;
        
        for(NSString *key in [result allKeys]) {
            NSLog(@"%@",[result objectForKey:key]);
        }
    };
    
    //*
    //**Bloque de fallo
    //*
    void (^fail)(NSError* error);
    fail = ^void(NSError* error){NSLog(@"error:%@",error);
        [MBProgressHUD hideHUDForView:[self view] animated:YES];
        
        
        [self dismissViewControllerAnimated:YES completion:^{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Beat Sender"
                                                           message:@"We lost the connection, try again"
                                                          delegate:nil
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles: nil];
            [alert show];
        }];
    };
    
    //*
    //**COMIENZA LA BUSQUEDA
    //*
    NSLog(@"Searching %@ with start=%d",searchTerm, start);
    [_rdio callAPIMethod:@"search" withParameters:params success:searchresult failure:fail];
    //[rdio callAPIMethod:@"search" withParameters:params success:searchresult failure:fail];
    
}


@end
