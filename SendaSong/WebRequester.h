//
//  WebRequester.h
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Protocol that helps to process the Web Result
 */
@protocol BSWebClientDelegate

@required
/**
 * Delegate that is called when the Webservice has been consumed by the client
 * @param jsonResult NSDictionary with the result of the Webservice cal
 * @param NSString with the name of the called method
 */
-(void) manageServiceResult:(NSDictionary*) jsonResult currentMethod:(NSString*) method;

@end

/**
 * CodeBlock that works as delegate too (as the protocol above)
 */
typedef void (^CompletionBlock)(NSDictionary *myJson, NSString* method, NSInteger statusCode);

/*
 * Interface
 */
@interface WebRequester : NSObject<NSURLConnectionDataDelegate>
{
    @private
        /** Where the result is saved */
        NSMutableData *receivedData;
}


/** HTTP Status response code */
@property (nonatomic) NSInteger httpStatusCode;
/** NSURL with the Web service URL */
@property (strong,nonatomic) NSURL* webServiceURL;
/** Method name of the Web service */
@property (weak,nonatomic) NSString* methodName;
/** Host of the Webservice */
@property (weak,nonatomic) NSString* host;
/** Delegate of the NSServiceResultDelegate protocol */
@property (assign,nonatomic) id<BSWebClientDelegate> resultDalegate;
/*  Delegate (as lambda expression in C#) */
@property (copy,nonatomic) CompletionBlock webserviceCallCompleted;
/**
 * Custom init
 * @param host
 * @param Method's name
 * @param Object that implements the NSServiceResultDelegate protocol
 */
-(id) initWithHost:(NSString*) host
            method:(NSString*) name
          delegate:(id<BSWebClientDelegate>) object
            params:(NSString*) firstParam, ... NS_REQUIRES_NIL_TERMINATION;

/**
 * Make a generic call to the specified web service method
 * @param valuesToSend NSDictionary with parammeters of the method
 * @param method Http Method
 */
-(void) makeGenericCall:(NSDictionary*) valuesToSend
             httpMethod:(NSString*) method;

/**
 * Class method that returns a NSString with the URL of the Web service
 */
+(NSString*) webServiceUrl;

/**
 * ApiKey of the webservice
 */
+(NSString*) apiKey;
@end