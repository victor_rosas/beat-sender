//
//  SearchServiceModel.m
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SearchServiceModel.h"

@implementation SearchServiceModel

-(void) SearchFriends: (NSString *)textToSearch
             Searcher: (NSString *)callerUserId
      completionBlock: (CompletionBlock)completionBlock
{
    NSString * queryString = [NSString stringWithFormat:@"search/friends?q=%@&uid=%@", textToSearch, callerUserId];
    
    WebRequester * requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                           method: queryString
                                                         delegate: nil
                                                           params: nil];
    
    [requester setWebserviceCallCompleted:completionBlock];
    [requester makeGenericCall:nil httpMethod:@"GET"];
}

-(void) SearchUsers: (NSString *)textToSearch
           Searcher: (NSString *)callerUserId
    completionBlock: (CompletionBlock)completionBlock
{
    NSString * queryString = [NSString stringWithFormat:@"search/users?q=%@&uid=%@", textToSearch, callerUserId];
    
    WebRequester * requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                           method: queryString
                                                         delegate: nil
                                                           params: nil];
    
    [requester setWebserviceCallCompleted:completionBlock];
    [requester makeGenericCall:nil httpMethod:@"GET"];
}


@end
