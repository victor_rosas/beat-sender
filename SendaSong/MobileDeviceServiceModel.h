//
//  MobileDeviceServiceModel.h
//  BeatSender
//
//  Created by isaac ojeda on 1/17/15.
//  Copyright (c) 2015 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRequester.h"

@interface MobileDeviceServiceModel : NSObject

/**
 * Registra el Id del dispositivo para futuras notificaciones
 * @param token Token generado por APN service
 * @param userId Id del usuario al que pertenece
 */
-(void) registerDeviceToken: (NSString*) token
                 withUserId:(NSString*) userId
            completionBlock:(CompletionBlock) block;

/**
 * Elimina el Push Token de la nube para no seguir recibiendo notificaciones Push
 * @param token Token generado por APN Service
 */
-(void) deleteDeviceToken:(NSString*) token
          completionBlock:(CompletionBlock) block;
@end
