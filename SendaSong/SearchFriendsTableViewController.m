//
//  SearchFriendsTableViewController.m
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SearchFriendsTableViewController.h"
#import "UICustomUserTableViewCell.h"
#import "SearchServiceModel.h"
#import "UserModel.h"   
#import "AppDelegate.h"
#import "BeatColorsHelper.h"
#import "UserServiceModel.h"
#import "SentSongsServiceModel.h"


@interface SearchFriendsTableViewController ()
{
    NSMutableArray * allFriends;    /** Arreglo con todos los amigos */
    NSArray        * searchResults; /** Arreglo con resultados de búsqueda */
    UserModel      * currentUser;   /** Usuario Actual Logueado */
    MBProgressHUD  * hud;           /** Muestra subview con loading message */
}

@end

@implementation SearchFriendsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - UI Events

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self->allFriends = [[NSMutableArray alloc]init];
    self->currentUser = [((AppDelegate*)[[UIApplication sharedApplication] delegate]) CurrentUser];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self->allFriends removeAllObjects];
    
    [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
    
    UserServiceModel *serviceModel = [[UserServiceModel alloc]init];
    
    [serviceModel getFriendsWithGuid:[self->currentUser UserId] completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
        //TODO: Handle response
        
        if (statusCode == 200) {
            NSArray* tempUsers = (NSArray*) myJson;
            
            for (int i = 0; i < [tempUsers count]; i++) {
                NSDictionary *userData = [tempUsers objectAtIndex:i];
                UserModel *newUser = [[UserModel alloc]initWithJson:userData];
                
                [self->allFriends addObject:newUser];
            }
            
            [[self tableView] reloadData];
        }
        
        [MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"UserName contains[c] %@", searchString];
    NSArray *temp = [NSArray arrayWithArray:self->allFriends];
    self->searchResults = [temp filteredArrayUsingPredicate:resultPredicate];

    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == [[self searchDisplayController] searchResultsTableView]) {
        return [self->searchResults count];
    }else{
        return [self->allFriends count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"SearchFriendIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:identifier];
    }
    
    UserModel *user = nil;
    
    if (tableView == [[self searchDisplayController] searchResultsTableView]) {
        user = [self->searchResults objectAtIndex:[indexPath row]];
    }else{
        user = [self->allFriends objectAtIndex:[indexPath row]];
    }

    [[cell textLabel] setText:[user UserName]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserModel *selectedUser;
    
    if (tableView == [[self searchDisplayController] searchResultsTableView]) {
        // lo seleccionó buscando
        selectedUser = [self->searchResults objectAtIndex:[indexPath row]];
    }else{
        // lo seleccionó en "todos"
        selectedUser = [self->allFriends objectAtIndex:[indexPath row]];
    }

    // TODO Enviar canción
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    hud = [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
    [hud setLabelText:@"Sending..."];
    [hud setDelegate:self];
    
    SentSongsServiceModel *serviceModel = [[SentSongsServiceModel alloc]init];
    
    [serviceModel PostSong:[self SelectedTrack]
              withSenderId:[self->currentUser UserId]
             andReceiverId:[selectedUser UserId]
               withMessage:[[self SelectedTrack] Message]
           completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
               
               //TODO Manage response and redirect to home
               NSLog(@"%@", myJson);
               NSLog(@"method %@ and status code %ld", method, (long)statusCode);

               [hud setMode:MBProgressHUDModeCustomView];
               [hud setCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]]];
               [hud setLabelText:@"Your Beat has been sent"];
               [hud hide:YES afterDelay:2];
    }];
    

}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UserModel *selectedUser = [self->allFriends objectAtIndex:[indexPath row]];
        [self->allFriends removeObjectAtIndex:[indexPath row]];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        UserServiceModel *serviceModel = [[UserServiceModel alloc]init];
        NSLog(@"Un following user %@, so sad :(", [selectedUser UserId]);
        
        [serviceModel unFollowUser:[self->currentUser UserId] withId:[selectedUser UserId] completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
            //TODO: ACTUALLY NOTHING TO DO
        }];
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView *) tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return UITableViewCellEditingStyleNone;
    }
    
    return UITableViewCellEditingStyleDelete;
}
#pragma mark MBProgressHUDDelegate methods
-(void) hudWasHidden:(MBProgressHUD *)hud
{
    int count = [[[self navigationController] viewControllers] count];
    [[self navigationController] popToViewController:[[[self navigationController]viewControllers]objectAtIndex:count - 3] animated:YES];
  
}
@end
