//
//  UserModel.h
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

/**
 * Id del Usuario
 */
@property (strong, nonatomic) NSString *UserId;
/**
 * Nombre de usuario
 */
@property (strong, nonatomic) NSString *UserName;
/**
 * Contraseña
 */
@property (strong, nonatomic) NSString *Password;
/**
 * Email del Usuario
 */
@property (strong, nonatomic) NSString *Email;
/**
 * Determina si el uysuario esta siendo seguido
 */
@property (assign) BOOL Following;
/**
 * Inicializa las dos propiedades del modelo User
 */
- (id) initWithNewData: (NSString*) userName
              password:(NSString*) password;

/**
 * Inicializa el objeto con tres propiedades
 */
- (id) initWithNewData:(NSString *)userName
              password:(NSString *)password
                 email:(NSString*) email;
/**
 * Inicializa el objeto desde JSON
 */
- (id) initWithJson:(NSDictionary*) data;
/**
 * Save the current values in UserPreferences.plist
 */
-(void) saveValues;

@end
