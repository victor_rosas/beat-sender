//
//  CreateSongViewController.h
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RdioTrack.h"

@interface CreateSongViewController : UIViewController<UITextFieldDelegate>
/**
 * Mensaje a mandar
 */
@property (weak, nonatomic) IBOutlet UITextField *txtMessage;
/**
 * Canción seleccionada en el Modal View
 */
@property (nonatomic, strong) RdioTrack *selectedTrack;
/**
 * Imagen que muestra el album de la canción
 */
@property (weak, nonatomic) IBOutlet UIImageView *albumImage;
/**
 * Label de la canción
 */
@property (weak, nonatomic) IBOutlet UILabel *trackLabel;
/**
 * Label del nombre del Album
 */
@property (weak, nonatomic) IBOutlet UILabel *albumLabel;
/**
 * Invoca de nuevo el View para buscar otra canción
 */
- (IBAction)btnCancel:(id)sender;
/**
 * Botón que pasa a la siguiente vista
 */
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
/**
 * Carga el detalle de la canción
 */
-(void) loadSongDetail;
/**
 * Se regresa al inbox
 */
-(void) startOver;

@end
