//
//  SongModel.m
//  SendaSong
//
//  Created by isaac ojeda on 29/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SongModel.h"

@implementation SongModel

-(id) initWithData:(NSDictionary *)json
{
    self = [super init];
    
    if (self) {
        [self setMessage:[json objectForKey:@"Message"]];
        [self setUserName:[json objectForKey:@"SenderUserName"]];
        [self setSentDate:[json objectForKey:@"SentDate"]];
        [self setSongId: [[json objectForKey:@"SentSongId"] integerValue]];
        [self setReceiverUserId:[json objectForKey:@"ReceiverUserId"]];
        [self setSenderUserId:[json objectForKey:@"SenderUserId"]];
        [self setIsReaded:[[json objectForKey:@"Readed"] boolValue]];
        
        // Rdio MetaData
        [self setRdioId:[[json objectForKey:@"SongMetaData"] objectForKey:@"RdioKey"]];
        [self setArtistName:[[json objectForKey:@"SongMetaData"] objectForKey:@"Artist"]];
        [self setSongName:[[json objectForKey:@"SongMetaData"] objectForKey:@"Name"]];
        [self setAlbumUrl:[[json objectForKey:@"SongMetaData"] objectForKey:@"AlbumUrl"]];
    }
    
    return self;
}

@end
