//
//  RegisterTableViewController.m
//  SendaSong
//
//  Created by isaac ojeda on 23/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "RegisterTableViewController.h"
#import "UserModel.h"
#import "AuthServiceModel.h"
#import "ValidationsHelper.h"   
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface RegisterTableViewController (){
    @private
        /** Determina si la Vista se encuentra actualmente en una llamada a la API */
        BOOL isBusy;
}

@end

@implementation RegisterTableViewController

#pragma mark - View Events
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isBusy = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView Delegates
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 1 && [indexPath row] == 0) {
        // TODO: Registrar usuario
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
        [self beginRegistrationProcess];
    }
}
#pragma mark - UITextFieldDelegate
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    // Caminito de los TextBoxs
    if ([textField isEqual:[self txtUserName]]) {
        [[self txtEmail] becomeFirstResponder];
    }else if( [textField isEqual:[self txtEmail]]){
        [[self txtPassword] becomeFirstResponder];
    }else {
        [textField resignFirstResponder];
        [self beginRegistrationProcess];
    }
        
    return YES;
}

#pragma mark - Private methods
/**
 * Inicia el proceso de autenticación
 */
-(void) beginRegistrationProcess{
    if (isBusy) {
        return;
    }
    
    // Validamos datos de entrada (igual serán validados por MVC)
    UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"Check the following fields" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    if ([[[self txtUserName] text] length] == 0 || [[[self txtEmail]text]length] == 0 || [[[self txtPassword] text]length] == 0) {
        // TODO Show ERROR
        [errorAlert setMessage:@"All the fields are required"];
        [errorAlert show];
        
        return;
    }
    
    if (![ValidationsHelper validateEmailFormat:[[self txtEmail] text]]) {
        [errorAlert setMessage:@"Enter a valid email please"];
        [errorAlert show];
        
        [[self txtEmail] becomeFirstResponder];
        return;
    }
    
    // Se inicia el proceso de Autenticación
    [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
    isBusy = YES;
    
    // Usuario con datos inicializados
    UserModel *newUser = [[UserModel alloc] initWithNewData:[[self txtUserName] text]
                                                   password:[[self txtPassword] text]
                                                      email:[[self txtEmail] text]];
    // Servicio de Autenticación
    AuthServiceModel * serviceModel = [[AuthServiceModel alloc] init];
    
    
    // Consumimos el servicio de registro de usuario y manejamos resultado
    [serviceModel registerNuewUser:newUser completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
        // TODO: Handle registration
        [MBProgressHUD hideHUDForView:[self view] animated:YES];
        NSLog(@"Respuesta con %ld", (long)statusCode);
        if (statusCode == 201) { // Created
            // TODO: Guardar usuario y pasar a Inbox
            // Guardamos los valores en localstorage
            UserModel *newUser = [[UserModel alloc] init];
            
            [newUser setUserId:[myJson objectForKey:@"UserId"]];
            [newUser setUserName: [myJson objectForKey:@"UserName"]];
            [newUser setEmail:[myJson objectForKey:@"Email"]];
            
            [newUser saveValues];
            
            // Guardamos la referencia del usuario
            AppDelegate *delegate =(AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate setCurrentUser:newUser];
            // Lo mandamos a la vista 'inbox'
            [self performSegueWithIdentifier:@"registerToInbox" sender:self];
        }else{
            // Ocurrió un error
            NSString* messageToShow = [ValidationsHelper getMessagesFromJson:myJson];
            
            // Mensaje de error al usuario
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Validation errors"
                                                                 message:messageToShow
                                                                delegate:nil
                                                       cancelButtonTitle:@"Try again"
                                                       otherButtonTitles:nil];
            [errorAlert show];
        }
        
        isBusy = NO;
    }];
}
@end
