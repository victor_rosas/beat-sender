//
//  SearchTrackTableViewController.h
//  BeatSender
//
//  Created by isaac ojeda on 10/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateSongViewController.h"
#import <Rdio/Rdio.h>

@interface SearchTrackViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,RdioDelegate>

/**
 * Controlador que lo manda a llamar
 */
@property (nonatomic, strong) CreateSongViewController *ParentController;
/**
 * Dismiss el modal
 */
- (IBAction)cancelClick:(id)sender;
/**
 * Referencia del Table View
 */
@property (strong, nonatomic) IBOutlet UITableView *tableView;

/**
 * Label de no resultados
 */
@property (weak, nonatomic) IBOutlet UILabel *noResultsLabel;
/**
 * Barra de búsqueda
 */
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

//

@end
