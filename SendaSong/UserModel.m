//
//  UserModel.m
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

#pragma mark - LifeCycle
-(id) init
{
    self=[super init];
    
    if(self){
        NSString *errorDesc=nil;
        NSPropertyListFormat format;
        NSString *plistPath=nil;
        // Get the path of UserPreferences.plist file
        NSString *rootPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        plistPath=[rootPath stringByAppendingPathComponent:@"UserPreferences.plist"];
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
            plistPath=[[NSBundle mainBundle] pathForResource:@"UserPreferences" ofType:@"plist"];
        }
        
        // Get the values and save'em in UserPreferences properties
        NSData *plistXML=[[NSFileManager defaultManager] contentsAtPath:plistPath];
        NSDictionary *temp=(NSDictionary*)[NSPropertyListSerialization propertyListFromData:plistXML mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&errorDesc];
        
        if(!temp){
            NSLog(@"Error reading plist: %@, format: %u",errorDesc,format);
        }
       
        // Seteamos los valores obtenidos
        [self setEmail:[temp objectForKey:@"UserEmail"]];
        [self setUserId:[temp objectForKey:@"UserId"]];
        [self setUserName:[temp objectForKey:@"UserName"]];
    }
    
    return self;
}

-(id) initWithNewData:(NSString *)userName
             password:(NSString *)password
{
    self = [super init];
    if (self) {
        // TODO set members
        [self setUserName:userName];
        [self setPassword:password];
    }
    
    return self;
}

-(id) initWithNewData:(NSString *)userName
             password:(NSString *)password
                email:(NSString *)email
{
    self = [super init];
    
    if (self) {
        // TODO Set members
        [self setUserName:userName];
        [self setPassword:password];
        [self setEmail:email];
    }
    return self;
}

-(id) initWithJson:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        [self setEmail:[data objectForKey:@"Email"]];
        [self setUserId:[data objectForKey:@"UserId"]];
        [self setUserName:[data objectForKey:@"UserName"]];
        [self setFollowing:[[data objectForKey:@"Following"] boolValue]];
    }
    
    return self;
}
#pragma mark - UserPreferences public methods
-(void) saveValues
{
    NSString *error=nil;
    NSString *rootPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath=[rootPath stringByAppendingPathComponent:@"UserPreferences.plist"];
    // Create a NSDictionary with new values
    NSDictionary *plistDict=[NSDictionary
                             dictionaryWithObjects:[NSArray arrayWithObjects:[self UserName], [self Email],[self UserId], nil]
                             forKeys:[NSArray arrayWithObjects:@"UserName", @"UserEmail",@"UserId", nil]];
    // Prepare the data and try to save it
    NSData *plistData=[NSPropertyListSerialization dataFromPropertyList:plistDict format:NSPropertyListBinaryFormat_v1_0 errorDescription:&error];
    
    if(plistData){
        [plistData writeToFile:plistPath atomically:YES];
    }else{
        NSLog(@"%@",error);
    }
    
}@end
