//
//  WebRequester.m
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "WebRequester.h"

@implementation WebRequester

#pragma mark - WebRequester instance methods
-(id) initWithHost:(NSString *)host
            method:(NSString *)name
          delegate:(id<BSWebClientDelegate>)object
            params:(NSString *)firstParam, ...
{
    // basic init
    self = [super init];
    
    if(!self)
    {
        return nil;
    }
    // custom init
    

    self.methodName = name;
    self.host = host;
    self.webServiceURL = [NSURL URLWithString:[self.host stringByAppendingFormat:@"/%@",self.methodName,nil]];
    
    // delegate to process the result depending the client implementation
    self.resultDalegate = object;
    
    return self;
}
-(void)makeGenericCall:(NSDictionary*) valuesToSend
            httpMethod:(NSString*) method
{
    [self makeCallWithValues:valuesToSend method:method];
}

#pragma mark - WebRequester private methods
-(void) makeCallWithValues:(NSDictionary*) dataToSend
                    method:(NSString*) method
{
    NSData *myDataJson;
    NSString *myJson;
    // Create the request
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:self.webServiceURL];
    
    if ([method isEqualToString:@"POST"]) {
        // Create a NSString with JSON Format from NSDictionary dataToSend pointer
        myDataJson = [NSJSONSerialization dataWithJSONObject:dataToSend options:NSJSONWritingPrettyPrinted error:nil];
        myJson =[[NSString alloc] initWithData:myDataJson encoding:NSUTF8StringEncoding];
        NSLog(@"JSON data to send %@ (%@)",myJson,self.methodName);
        
        // Headers for the request if POST
        NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[myJson length]];
        [theRequest addValue:msgLength forHTTPHeaderField:@"Content-Length"];
        
        // Data for the request if POST
        [theRequest setHTTPBody:[myJson dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Headers for the request
    [theRequest addValue:@"application/json"
      forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue:[NSString stringWithFormat:@"ApiKey=%@",[WebRequester apiKey]]
      forHTTPHeaderField:@"Authorization"];
    [theRequest setHTTPMethod:method];
            
    // Create the connectino with the request and start loading the data
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if(theConnection)
    {
        // Craete the NSMutableData to hold the received data
        receivedData = [NSMutableData data];
    }
    
}

#pragma mark NSURLConnectionDataDelegate methods
- (void)connection:(NSURLConnection *)connection
didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    [self setHttpStatusCode:[httpResponse statusCode]];
    
    [receivedData setLength:0];
}
-(void)connection:(NSURLConnection *)connection
   didReceiveData:(NSData *)data
{
    // Get the data received from the service
    [receivedData appendData:data];
}
-(void)connection:(NSURLConnection *)connection
 didFailWithError:(NSError *)error
{
    // Notify the error
    NSLog(@"Connection failed! Error / %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLErrorKey]);
    
    // Anyways we trigger the delegates
    if (self.resultDalegate) {
        [self.resultDalegate manageServiceResult:nil currentMethod:self.methodName];
    }
    
    if (self.webserviceCallCompleted) {
        self.webserviceCallCompleted(nil, self.methodName, self.httpStatusCode);
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Succeded! Received %lu bytes of data (%@)",(unsigned long)[receivedData length],self.methodName);
    // json parsing to NSDictionary
    NSError *error = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:&error];

    
    // call the delegate of the implemented protocol (as C# interface)
    if (self.resultDalegate) {
        [self.resultDalegate manageServiceResult:jsonData currentMethod:self.methodName];
    }
    // as above, call the delegate (as delegate in C#)
    if (self.webserviceCallCompleted) {
        self.webserviceCallCompleted(jsonData,self.methodName, self.httpStatusCode);
    }

}

#pragma mark - Class methods
+(NSString*)webServiceUrl
{
    //return @"http://192.168.0.200/SendaSongAPI/api/";
    return @"http://beatsenderapidev.azurewebsites.net/api";
}
+(NSString*)apiKey
{
    
    return @"test";
    //return @"aEg6YVZRF6B3Rlxg4IdUPw";
}

@end
