//
//  BeatColorsHelper.m
//  BeatSender
//
//  Created by isaac ojeda on 19/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "BeatColorsHelper.h"

@implementation BeatColorsHelper

+(UIColor*) mainColor
{
    return [UIColor colorWithRed:144.0/255.0 green:14.0/255.0 blue:142.0/255.0 alpha:1];
}

+(UIColor*) secondaryColor
{
    return [UIColor colorWithRed:198/255.0f green:95/255.0f blue:197/255.0f alpha:1.0f];
}

+(UIColor*) beatBlueColor
{
    return [UIColor colorWithRed:0/255.0f green:249/255.0f blue:252/255.0f alpha:1];
}
@end
