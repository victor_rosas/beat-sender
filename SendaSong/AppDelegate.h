//
//  AppDelegate.h
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"
#import <Rdio/Rdio.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

//+ (Rdio *)rdioInstance;
+ (Rdio *)sharedRdio;

@property (strong, nonatomic) UIWindow *window;
/**
 * Usuario Loggeado
 */
@property (strong, nonatomic) UserModel *CurrentUser;
/**
 * Es el Push token actual del dispositivo
 */
@property (strong, nonatomic) NSString *CurrentPushToken;
@property(readonly) Rdio *rdio;

@end
