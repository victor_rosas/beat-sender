//
//  UICustomTrackTableViewCell.m
//  BeatSender
//
//  Created by isaac ojeda on 10/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "UICustomTrackTableViewCell.h"

@implementation UICustomTrackTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
