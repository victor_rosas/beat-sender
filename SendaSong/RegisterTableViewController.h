//
//  RegisterTableViewController.h
//  SendaSong
//
//  Created by isaac ojeda on 23/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterTableViewController : UITableViewController<UITextFieldDelegate>

/** Textbox del nombre de usuario */
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
/** Textbox del email */
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
/** Textbox del password */
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;


@end
