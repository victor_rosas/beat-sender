//
//  ValidationsHelper.m
//  SendaSong
//
//  Created by isaac ojeda on 23/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "ValidationsHelper.h"

@implementation ValidationsHelper

# pragma mark - Class Public Methods
+(BOOL) validateEmailFormat:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

+(NSString*) getMessagesFromJson:(NSDictionary *)myJson
{
    // Ocurrió un error
    NSString *errorTitle = [myJson objectForKey:@"Message"];
    // Si existe 'Error' es un error personalizado, si es de MVC vendrá todo dentro de ModelState
    NSArray* arrayMessages = [[myJson objectForKey:@"ModelState"] objectForKey:@"Error"];
    
    if (!arrayMessages) {
        // Si es error de MVC los mostramos todos
        NSDictionary* validationData = [myJson objectForKey:@"ModelState"];
        // Obtenemos todos los errores como un array de NSDictionaries
        arrayMessages = [validationData allValues];
    }
    
    // Reemplazamos los parentesis y le hacemos join con un ' ' (espacio)
    NSString *messageToShow = [[[arrayMessages componentsJoinedByString:@" "]
                                stringByReplacingOccurrencesOfString:@"(" withString:@""]
                               stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSLog(@"Título: %@", errorTitle);
    NSLog(@"Errores de validación: %@", messageToShow);
    
    return messageToShow;
}

@end
