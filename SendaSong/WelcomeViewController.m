//
//  ViewController.m
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "WelcomeViewController.h"
#import "UserModel.h"
#import "AppDelegate.h"
#import "BeatColorsHelper.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Verificamos si es un usuaro que ya está logueado
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    UserModel *currentUser = [[UserModel alloc]init];
    
    // necesito el tamaño del teclado
    // al inicio obtenemos el tamaño del keyboard
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
    
    if (![[currentUser UserId] isEqualToString:@"-1"]) {
        NSLog(@"El usuario ya está logueado (%@)", [currentUser Email]);
        
        [appDelegate setCurrentUser:currentUser];
        // Nos vamos a la vista 'inbox'
        [self performSegueWithIdentifier:@"welcomeToInbox" sender:self];
    }else{
        NSLog(@"El usuario no está logueado");
    }
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    
    [[UINavigationBar appearance] setBarTintColor: [BeatColorsHelper mainColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName : [UIFont fontWithName:@"Bangla Sangam MN" size:23.0]}];
//    [[UISearchBar appearance] setBarTintColor: [BeatColorsHelper secondaryColor]];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark Private
-(void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    NSLog(@"keyboardFrame: %@", NSStringFromCGRect(keyboardFrame));
}

@end
