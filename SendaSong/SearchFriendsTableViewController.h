//
//  SearchFriendsTableViewController.h
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "RdioTrack.h"

@interface SearchFriendsTableViewController : UITableViewController<MBProgressHUDDelegate>

/**
 * Referencia a la barra de búsqueda
 */
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
/**
 * Canción seleccionada a enviar
 */
@property (strong,nonatomic) RdioTrack * SelectedTrack;

@end
