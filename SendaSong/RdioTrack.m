//
//  RdioTrack.m
//  BeatSender
//
//  Created by isaac ojeda on 12/27/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "RdioTrack.h"

@implementation RdioTrack

-(id)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        //TODO init object
        [self setTrackId:[json objectForKey:@"key"]];
        [self setTrackName:[json objectForKey:@"name"]];
        [self setArtistName:[json objectForKey:@"artist"]];
        [self setAlbumName:[json objectForKey:@"album"]];
        [self setAlbumImageURL:[[NSURL alloc] initWithString:[json objectForKey:@"icon"]]];
        [self setAlbumImageURL400:[[NSURL alloc] initWithString:[json objectForKey:@"icon400"]]];
        
        [self setIsLoaded:NO];
    }
    
    return self;
}



@end
