//
//  CreateSongViewController.m
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "CreateSongViewController.h"
#import "SearchTrackViewController.h"
#import "SearchFriendsTableViewController.h"
#import "MBProgressHUD.h"
#import "QuartzCore/CALayer.h"
#import <Rdio/Rdio.h>
#import "BeatColorsHelper.h"
#import "ClientCredentials.h"
#import "AppDelegate.h"


@interface CreateSongViewController ()
{
    BOOL    isFirstTime;   /** Determina si es la primera ves que se muestra la vista */
    Rdio  * rdio;          /** Wrapper de la API de Rdio */
    Rdio  * _rdio;
    CGRect  keyboardFrame; /** Tamaño del teclado */
}

@end

@implementation CreateSongViewController

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        isFirstTime = YES;
    }
    
    return self;
}
#pragma mark - UI Events
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [[[self albumImage]layer]setShadowColor:[[UIColor blackColor] CGColor]];
    [[[self albumImage]layer]setShadowRadius:8.0];
    [[[self albumImage]layer]setShadowOffset:CGSizeMake(0.0, 5.0)];
    [[[self albumImage]layer]setShadowOpacity:0.5];
    [[self albumImage]setClipsToBounds:NO];
    
    // rdio wrapper init ...
    self->rdio = [[Rdio alloc] initWithClientId:@"d5t3ybdaq5ejxfprkgfqazxsla"//CLIENT_ID
                                      andSecret:@"aEg6YVZRF6B3Rlxg4IdUPw"//CLIENT_SECRET
                                       delegate:nil];
    
    // Change button color
    [[self btnNext] setBackgroundColor: [BeatColorsHelper mainColor]];
    
    // necesito el tamaño del teclado
    // al inicio obtenemos el tamaño del keyboard
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _rdio = [AppDelegate sharedRdio];
    [_rdio setDelegate:self];
    
    if (self->isFirstTime) {
        [self performSegueWithIdentifier: @"createToSearch"
                                  sender: self];
        
        self->isFirstTime = NO;
    }

}
-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        if ([[self->rdio player]state] == RDPlayerStatePlaying) {
            [[self->rdio player]stop];
        }
    }
    [super viewWillDisappear:animated];
}
-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnCancel:(id)sender {
    [self setSelectedTrack:nil];
    [self performSegueWithIdentifier:@"createToSearch"
                              sender:self];
}
#pragma mark - Public Methods
-(void) loadSongDetail
{
    // album que ya esta cargado, se cargará despues uno con mejor resolución
    [[self albumImage] setImage:[[self selectedTrack]AlbumImage]];
    
    [[self trackLabel] setText:[[self selectedTrack]TrackName]];
    [[self albumLabel] setText:[[self selectedTrack]AlbumName]];
    [[self navigationItem] setTitle:[[self selectedTrack]ArtistName]];
    
    [[self albumImage] setHidden:NO];
    [[self trackLabel] setHidden:NO];
    [[self albumLabel] setHidden:NO];
    
    //TODO Cargar esta imagen ASYNC
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //TODO: Load image async
        NSData *imageData = [NSData dataWithContentsOfURL:[[self selectedTrack]AlbumImageURL400]];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [[self albumImage]setImage: [UIImage imageWithData:imageData]];
        });
    });
    // Reproducir canción
    [[self->rdio player] play/*playSource*/:[[self selectedTrack] TrackId]];
}

-(void)startOver
{
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegates

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:[self txtMessage]]) {
        [[self txtMessage]resignFirstResponder];
    }
    return YES;
}
/**
 *
 */
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:[self txtMessage] up:YES];
}
/**
 *
 */
-(void) textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:[self txtMessage] up:NO];
}
/**
 *
 */
-(void) animateTextField:(UITextField*) textField up:(BOOL) up
{
    const int movementDistance = self->keyboardFrame.size.height;
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    [[self view] setFrame:CGRectOffset([[self view] frame], 0, movement)];
    
    [UIView commitAnimations];
}

#pragma mark - UI Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepare For Segue in Create Song View");
    
    RDPlayerState state = [[self->rdio player] state];
    
    if (state == RDPlayerStatePlaying) {
        [[self->rdio player]stop];
    }
    
    if ([[segue identifier] isEqual:@"createToSearch"]) {
        // TODO: Save Controller reference in new controller
        
        SearchTrackViewController *destinationController = (SearchTrackViewController*)[segue destinationViewController];
        
        [destinationController setParentController:self];
    }else if([[segue identifier] isEqual:@"createToSearchFriends"]){
        // TODO: Save selected song reference in new controller
        SearchFriendsTableViewController *destinationController = (SearchFriendsTableViewController*) [segue destinationViewController];
        // Set the message if exists

        if ([[self txtMessage] text]) {
            [[self selectedTrack] setMessage:[[self txtMessage] text]];
        } else{
            [[self selectedTrack] setMessage:@"Hey, I just send you this song"];
        }
        
        [destinationController setSelectedTrack:[self selectedTrack]];
    }
}
#pragma - mark Private
-(void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect frame = [[self view] convertRect:rawFrame fromView:nil];
    
    if (self->keyboardFrame.size.height <= 0) {
        self->keyboardFrame = frame;
    }
    
    
    NSLog(@"keyboardFrame: %@", NSStringFromCGRect(frame));
}

@end
