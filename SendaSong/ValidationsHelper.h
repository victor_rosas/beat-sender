//
//  ValidationsHelper.h
//  SendaSong
//
//  Created by isaac ojeda on 23/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Clase con varios helpers para validar
 */
@interface ValidationsHelper : NSObject


/**
 * Valida que un correo electrónico sea válido
 */
+(BOOL) validateEmailFormat: (NSString*) email;
/**
 * De los errores que regresa MVC (ModelState) se obtienen
 * todos los errores concatenados
 */
+(NSString*) getMessagesFromJson: (NSDictionary*) myJson;
@end
