//
//  SettingsTableViewController.m
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "AppDelegate.h"
#import "UserModel.h"
#import "MobileDeviceServiceModel.h"

@interface SettingsTableViewController ()
{
    @private
        /** AppDelegate */
        AppDelegate *appDelegate;
}

/**
 * Celda que contiene el email del usuario
 */
@property (weak, nonatomic) IBOutlet UITableViewCell *cellUserEmail;
/**
 *  Celda que contiene el nombre de usuario
 */
@property (weak, nonatomic) IBOutlet UITableViewCell *cellUserName;

@end

@implementation SettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - View Events
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Obtenemos la info del usuario logueado
    appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    UserModel *currentUser = [appDelegate CurrentUser];
    // Los ponemos en las celdas
    [[[self cellUserEmail] detailTextLabel] setText:[currentUser Email]];
    [[[self cellUserName]detailTextLabel]setText:[currentUser UserName]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableView Events
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Segunda sección
    if ([indexPath section] == 1 && [indexPath row] == 0) {

        // Logout
        [appDelegate setCurrentUser:nil];
        UserModel *removeUser = [[UserModel alloc] init];
        
        [removeUser setUserId:@"-1"];
        [removeUser setUserName:@""];
        [removeUser setEmail:@""];
            
        [removeUser saveValues];
        
        
        //Eliminamos el pushToken
        NSString *tokenToDelete = [appDelegate CurrentPushToken];
        MobileDeviceServiceModel *serviceModel = [[MobileDeviceServiceModel alloc] init];
        
        [serviceModel deleteDeviceToken:tokenToDelete completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
            NSLog(@"Se ha eliminado");
        }];
        
        
        
        // Nos vamos a la vista inicial
        [[self navigationController] popToViewController:[[[self navigationController] viewControllers] objectAtIndex:0] animated:YES];
    }
}

@end
