
//
//  RdioTrack.h
//  BeatSender
//
//  Created by isaac ojeda on 12/27/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RdioTrack : NSObject

/**
 * Id del Track en Rdio
 */
@property (strong, nonatomic) NSString *TrackId;
/**
 * Nombre de la canción
 */
@property (strong, nonatomic) NSString *TrackName;
/**
 * Mensaje a enviar
 */
@property (strong, nonatomic) NSString *Message;
/**
 * Nombre del artista
 */
@property (strong, nonatomic) NSString *ArtistName;
/**
 * Nombre del Album
 */
@property (strong, nonatomic) NSString *AlbumName;
/**
 * URL de la imagen del album 200*200
 */
@property (strong, nonatomic) NSURL * AlbumImageURL;
/**
 * Nombre del album 400*400
 */
@property (strong, nonatomic) NSURL * AlbumImageURL400;
/**
 * Imagen del Album
 */
@property (strong, nonatomic) UIImage * AlbumImage;
/**
 * Determina si ya se cargó el album
 */
@property (assign) BOOL IsLoaded;
/**
 * Inicializa el elemento con datos de un JSON
 */
-(id)initWithJson:(NSDictionary*)json;



@end
