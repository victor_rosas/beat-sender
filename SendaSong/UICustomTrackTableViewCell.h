//
//  UICustomTrackTableViewCell.h
//  BeatSender
//
//  Created by isaac ojeda on 10/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICustomTrackTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;
@property (weak, nonatomic) IBOutlet UILabel *trackLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;

@end
