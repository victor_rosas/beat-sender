//
//  SongModel.h
//  SendaSong
//
//  Created by isaac ojeda on 29/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SongModel : NSObject

-(id) initWithData: (NSDictionary*) json;

/**
* Mensaje de la dedicatoria
*/
@property (strong, nonatomic) NSString* Message;
/*
* Nombre quien dedica la canción
*/
@property (strong, nonatomic) NSString* UserName;
/**
* Fecha en la que se envió la dedicatoria
*/
@property ( strong, nonatomic) NSString* SentDate;
/**
 * Id de la dedicatoria
 */
@property (assign) NSInteger SongId;
/**
 * Id del usuario que la recibe
 */
@property (strong, nonatomic) NSString *ReceiverUserId;
/*
 * Id del usuario que la manda
 */
@property (strong, nonatomic) NSString *SenderUserId;
/**
 * Determina si se ha leido el mensaje
 */
@property (assign) BOOL IsReaded;
/**
 *
 */
@property (strong, nonatomic) NSString *RdioId;
/**
 *
 */
@property (strong, nonatomic) NSString *ArtistName;
/**
 *
 */
@property (strong, nonatomic) NSString *SongName;
/**
 *
 */
@property (strong, nonatomic) NSString *AlbumUrl;
@end
