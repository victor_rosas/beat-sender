//
//  InboxTableViewController.h
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"
#import "SongModel.h"
#import <Rdio/Rdio.h>

@interface InboxTableViewController : UITableViewController
{
    

    /** Usuario actualmente logueado */
    UserModel *currentUser;
    /** Canciones dedicadas */
    NSMutableArray *songs;
    /** Selected Song*/
    SongModel *selectedSong;
}

    /** Table View que tiene las canciones */
    @property (strong, nonatomic) IBOutlet UITableView *songsTableView;

@end
