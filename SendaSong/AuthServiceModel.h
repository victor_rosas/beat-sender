//
//  AuthServiceModel.h
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRequester.h"
#import "UserModel.h"

@interface AuthServiceModel : NSObject


/**
 * Manda solicitud POST para autenticar un usuario
 * @param user Instancia del usuario
 * @completionBlock Callback con los resultados
 */
- (void) authenticateUserModel: (UserModel*) user
               completionBlock: (CompletionBlock) completionBlock;
/**
 * Registra un nuevo usuario
 * @param newUser Nuevo usuario con password, username y email
 * @completionBlock Callback con los resultados
 */
- (void) registerNuewUser: (UserModel*) newUser
          completionBlock:(CompletionBlock) completionBlock;
@end
