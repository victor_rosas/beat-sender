//
//  LoginViewController.h
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthServiceModel.h"

@interface LoginTableViewController : UITableViewController<UITextFieldDelegate>

@end
