//
//  LoginViewController.m
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "LoginTableViewController.h"
#import "MBProgressHUD.h"
#import "UIBAlertView.h"
#import "AppDelegate.h"
#import "ValidationsHelper.h"

@interface LoginTableViewController ()

/**
 * Textbox del usuario
 */
@property (weak, nonatomic) IBOutlet UITextField *txtUser;
/**
 * Textbox del Password (secure)
 */
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;


@end

@implementation LoginTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - View events
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate Method
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if([self txtUser]==textField){
        [[self txtPassword] becomeFirstResponder];
    }else if([self txtPassword]==textField){
        [self beginLoginProcess];
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - Table Delegates
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 1 && [indexPath row] == 0) {
        [self beginLoginProcess];
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    }
}

#pragma mark - Privat methods
/**
 * Inicia el proceso de iniciar sesión
 */
-(void) beginLoginProcess
{
    NSString *user = [[self txtUser] text];
    NSString *password = [[self txtPassword] text];
    
    
    UserModel *userToAuth = [[UserModel alloc] initWithNewData:user password:password];
    AuthServiceModel *serviceModel = [[AuthServiceModel  alloc] init];
    
    // Consumimos el Webservice para autenticar
    [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
    
    [serviceModel authenticateUserModel:userToAuth
                        completionBlock:^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
        
        // Ocultamos el progress View
        [MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
        
        // Obtenemos la respuesta
        NSLog(@"Finalizado con status %ld", (long)statusCode);
        
        if (statusCode == 200) {
            // Auth Success
            NSDictionary *userData = [myJson objectForKey:@"User"];
            
            // Guardamos los valores en localstorage
            UserModel *newUser = [[UserModel alloc] init];
            
            [newUser setUserId:[userData objectForKey:@"UserId"]];
            [newUser setUserName: [userData objectForKey:@"UserName"]];
            [newUser setEmail:[userData objectForKey:@"Email"]];
            
            [newUser saveValues];
            
            // Guardamos la referencia del usuario
            AppDelegate *delegate =(AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate setCurrentUser:newUser];
            [self performSegueWithIdentifier:@"loginToInbox" sender:self];
        }else{
            // Ocurrió un error
            NSString* messageToShow = [ValidationsHelper getMessagesFromJson:myJson];
            
            // Mensaje de error al usuario
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Validation Errors"
                                                                 message:messageToShow
                                                                delegate:nil
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil];
            [errorAlert show];
        }
    }];
}
@end
