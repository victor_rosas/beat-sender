//
//  UserServiceModel.h
//  BeatSender
//
//  Created by isaac ojeda on 03/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRequester.h"

@interface UserServiceModel : NSObject

/**
 * Sigue un nuevo usuario
 * @param followingId Id del usuario a seguir
 * @param followerId Id del usuario que sigue
 * @param completionBlock Callback al finalizar la llamada
 */
-(void) followUser:(NSString*) followingId
            withId:(NSString*) followerId
   completionBlock:(CompletionBlock) completionBlock;
/**
 * Se trae a todos los amigos del usuario
 * @param userId Id del usuario logueado
 */
-(void) getFriendsWithGuid:(NSString*) userId
           completionBlock:(CompletionBlock) completionBlock;
/**
 * Deja de seguir a un usuario
 * @param followingId Id del usuario a dejar de seguir
 * @param followerId Id del usuario seguidor :O
 * @param completionBlock Callback al finalizar la llamada
 */
-(void) unFollowUser:(NSString*) followingId
              withId:(NSString*) followerId
     completionBlock:(CompletionBlock) completionBlock;

@end
