//
//  SearchServiceModel.h
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRequester.h"
@interface SearchServiceModel : NSObject

/**
 * Busca usuarios que el usuario sigue
 * @param textToSearch texto a buscar
 * @param callerUserId Usuario que está buscando
 * @param completionBlock Callback que se ejecuta al terminar la llamada
 */
-(void) SearchFriends: (NSString*)textToSearch
             Searcher: (NSString*) callerUserId
      completionBlock: (CompletionBlock) completionBlock;

/**
 * Busca usuarios que usan la App sin importar si se siguen
 * @param textToSearch Texto a buscar
 * @param callerUserId Usuario que está buscando
 * @param completionBlock Callback que se ejecuta al terminar la llamada
 */
-(void) SearchUsers: (NSString*) textToSearch
           Searcher: (NSString*) callerUserId
    completionBlock: (CompletionBlock) completionBlock;
@end
