//
//  AuthServiceModel.m
//  SendaSong
//
//  Created by isaac ojeda on 22/03/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "AuthServiceModel.h"

@implementation AuthServiceModel



#pragma mark - Public Metods
- (void) authenticateUserModel:(UserModel *)user
               completionBlock: (CompletionBlock) completionBlock
{
    WebRequester * requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                           method:@"auth"
                                                         delegate:nil
                                                           params: nil];
    // Callback al terminar de obtener la respuesta
    [requester setWebserviceCallCompleted:completionBlock];
    // Datos a convertir a JSON
    NSArray *keys = [[NSArray alloc] initWithObjects:@"User",@"Password", nil];
    NSArray *values = [[NSArray  alloc] initWithObjects:[user UserName], [user Password], nil];
    
    NSDictionary *dataToSend = [[NSDictionary alloc] initWithObjects:values
                                                             forKeys:keys];
    
    [requester makeGenericCall:dataToSend httpMethod:@"POST"];
}

- (void) registerNuewUser:(UserModel *)newUser
          completionBlock:(CompletionBlock)completionBlock
{
    WebRequester *requester = [[WebRequester alloc] initWithHost:[WebRequester webServiceUrl]
                                                          method:@"users"
                                                        delegate:nil
                                                          params:nil];
    // Set de callback
    [requester setWebserviceCallCompleted:completionBlock];
    // Datos a convertir a JSON
    NSArray *keys = [[NSArray alloc] initWithObjects:@"UserName", @"Email", @"PasswordHash", nil];
    NSArray *values = [[NSArray alloc] initWithObjects: [newUser UserName], [newUser Email], [newUser Password], nil];
    NSDictionary *dataToSend = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
    
    [requester makeGenericCall:dataToSend httpMethod:@"POST"];
}
@end
