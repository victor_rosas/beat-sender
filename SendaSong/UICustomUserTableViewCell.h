//
//  UICustomUserTableViewCell.h
//  BeatSender
//
//  Created by isaac ojeda on 03/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface UICustomUserTableViewCell : UITableViewCell

@property (nonatomic,strong) UserModel* UserToFollow;
@property (nonatomic, strong) UserModel *UserFollowing;
@property (weak, nonatomic) IBOutlet UILabel *lblUser;
@property (weak, nonatomic) IBOutlet UIButton *btnFollow;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowing;
- (IBAction)btnFollowClick:(id)sender;
/**
 * Prepara la celda custom para su uso
 */
-(void) prepareCellForUserAdd;

@end
