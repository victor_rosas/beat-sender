//
//  SearchUsersTableViewController.m
//  BeatSender
//
//  Created by isaac ojeda on 01/05/14.
//  Copyright (c) 2014 Isaac Ojeda. All rights reserved.
//

#import "SearchUsersTableViewController.h"
#import "UserModel.h"
#import "AppDelegate.h"
#import "SearchServiceModel.h"
#import "UICustomUserTableViewCell.h"
#import "MBProgressHUD.h"

@interface SearchUsersTableViewController ()
{
    
    NSMutableArray *searchResults; /** Arreglo con resultados de búsqueda */
    UserModel      *currentUser;   /** Usuario Actual Logueado */
}

@end

@implementation SearchUsersTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self->searchResults = [[NSMutableArray alloc] init];
    self->currentUser = [((AppDelegate*)[[UIApplication sharedApplication] delegate]) CurrentUser];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UI Search Bar Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
    
    [searchBar resignFirstResponder];
    SearchServiceModel *serviceModel = [[SearchServiceModel alloc]init];
    
    [serviceModel SearchUsers: [searchBar text]
                     Searcher: [self->currentUser UserId]
              completionBlock: ^(NSDictionary *myJson, NSString *method, NSInteger statusCode) {
                  
                  [self->searchResults removeAllObjects];
                  
                  if (statusCode == 200) {
                      
                      NSArray* tempUsers =(NSArray*) myJson;
                      for (int i = 0; i < [tempUsers count]; i++) {
                          NSDictionary *userData = [tempUsers objectAtIndex:i];
                          UserModel *newUser = [[UserModel alloc]initWithJson:userData];
                          
                          [self->searchResults addObject:newUser];
                      }
                      
                      NSLog(@"Se encontraron %lu usuarios",(unsigned long)[self->searchResults count]);
                  }
                  
                  [self.tableView reloadData];
                  [MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
              }];
    
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self->searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"SearchUserIdentifier";
    UICustomUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UICustomUserTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                reuseIdentifier:identifier];
    }
    
    UserModel *user = [self->searchResults objectAtIndex:[indexPath row]];
    
    [[cell lblUser]setText:[user UserName]];
    
    [cell setUserToFollow:user];
    [cell setUserFollowing:self->currentUser];
    [cell prepareCellForUserAdd];
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
